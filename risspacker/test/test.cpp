// Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
// This file is part of the FFD-Packer project, released under the BSD 3-Clause License.

#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "../src/risspacker/Node.hpp"
#include "../src/risspacker/Node.cpp"
#include "../src/risspacker/BinPacker.hpp"
#include "../src/risspacker/RectCollection.hpp"
#include "../src/risspacker/RectCollection.cpp"
#include "../src/risspacker/RISSPacker.hpp"
#include "../src/risspacker/RISSPacker.cpp"
#include "catch.hpp"

TEST_CASE("Unit tests for RectCollection", "RISSPacker test")
{

    SECTION("Exception thrown for negative X rectangle input dimension")
    {
        auto collection = RectCollection();
        CHECK_THROWS(collection.append(-1,1));
    }

    SECTION("Exception thrown for negative X rectangle input dimension")
    {
        auto collection = RectCollection();
        CHECK_THROWS(collection.append(1,-1));
    }
}

TEST_CASE("Unit tests BinPacker", "RISSPacker test")
{
    const auto BIN_DIM = 1.0;
    auto packer = BinPacker<Rect, Shelf>(BIN_DIM);
    std::vector<Rect> rects;

    rects.push_back(Rect(0.98, 0.98));
    rects.push_back(Rect(0.95, 0.95));

    auto shelfPacker = BinPacker<Shelf, Shelves>(BIN_DIM);

    SECTION("Unit tests of BinPacker methods, pack(), extractTree(), and add()")
    {
        packer.pack(rects);
        CHECK(packer.bins.size() == 2);

        auto gap = BIN_DIM;
        for (const auto& rect : packer.bins[0].items) {
            gap -= rect.width;
        }
        CHECK(gap == Approx(0.02));

        gap = BIN_DIM;
        for (const auto& rect : packer.bins[1].items) {
            gap -= rect.width;
        }
        CHECK(gap == Approx(0.05));

        for (auto& shelf : packer.bins) {
            shelf.width = shelf.items[0].height;
        }

        shelfPacker.pack(packer.bins);
        CHECK(shelfPacker.bins.size() == 2);
    }

    SECTION("Unit test of BinPacker method extractTree()")
    {
        rects.push_back(Rect(0.97, 0.97));
        rects.push_back(Rect(0.025, 0.025));

        packer.pack(rects);

        auto gap = BIN_DIM;
        for (const auto& rect : packer.bins[2].items) {
            gap -= rect.width;
        }

        CHECK(packer.bins.size() == 3);
        CHECK(gap == Approx(0.005));

        for (auto& shelf : packer.bins) {
            shelf.width = shelf.items[0].height;
        }
        shelfPacker.pack(packer.bins);
        CHECK(shelfPacker.bins.size() == 3);
    }

    SECTION("Unit test of BinPacker method redBlackConstraint()")
    {
        rects.push_back(Rect(0.99, 0.99));
        rects.push_back(Rect(0.95, 0.95));
        rects.push_back(Rect(0.95, 0.95));
        rects.push_back(Rect(0.005, 0.005));
        packer.pack(rects);

        auto gap = BIN_DIM;
        for (const auto& rect : packer.bins[2].items) {
            gap -= rect.width;
        }
        CHECK(gap == Approx(0.005));

        CHECK(packer.bins.size() == 5);

        for (auto& shelf : packer.bins) {
            shelf.width = shelf.items[0].height;
        }
        shelfPacker.pack(packer.bins);
        CHECK(shelfPacker.bins.size() == 5);
    }

    SECTION("Unit test of BinPacker method updateTree()")
    {
        rects.push_back(Rect(0.99, 0.99));
        rects.push_back(Rect(0.95, 0.95));
        rects.push_back(Rect(0.95, 0.95));
        rects.push_back(Rect(0.045, 0.01));
        rects.push_back(Rect(0.045, 0.01));
        rects.push_back(Rect(0.045, 0.01));
        packer.pack(rects);

        auto gap = BIN_DIM;
        for (const auto& rect : packer.bins[1].items) {
            gap -= rect.width;
        }
        CHECK(gap == Approx(0.005));

        gap = BIN_DIM;
        for (const auto& rect : packer.bins[3].items) {
            gap -= rect.width;
        }
        CHECK(gap == Approx(0.005));

        gap = BIN_DIM;
        for (const auto& rect : packer.bins[4].items) {
            gap -= rect.width;
        }
        CHECK(gap == Approx(0.005));

        CHECK(packer.bins.size() == 5);
        for (auto& shelf : packer.bins) {
            shelf.width = shelf.items[0].height;
        }

        shelfPacker.pack(packer.bins);
        CHECK(shelfPacker.bins.size() == 5);
    }
}