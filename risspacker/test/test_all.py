import pytest
import risspacker as riss
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
	
def test_20000():
    collection = riss.RectCollection()
    with open(os.path.join(dir_path, "build/uniform20000.txt")) as fp:
        lines = fp.readlines()
        for line in lines:
            line = line.split()
            collection.append(float(line[0]), float(line[1]))
    assert collection.maxDim() == pytest.approx(0.9999, 0.0001)
    assert abs(collection) == pytest.approx(4999, 0.1)
    square = riss.RISSPacker(collection.cpp_collection) ##todo: could I put this RISSPacker() (perhaps renamed to packer) inside risspacker directly?
    assert abs(square) == pytest.approx(5183.89, 0.01)
	
def test_160000():
    collection = riss.RectCollection()
    with open(os.path.join(dir_path, "build/uniform160000.txt")) as fp:
        lines = fp.readlines()
        for line in lines:
            line = line.split()
            collection.append(float(line[0]), float(line[1]))
    assert collection.maxDim() == pytest.approx(0.9999, 0.0001)
    assert abs(collection) == pytest.approx(39892.5, 0.1)
    square = riss.RISSPacker(collection.cpp_collection) 
    assert abs(square) == pytest.approx(40000, 1) #may as well do exact test??
	
def test_1600():
    collection = riss.RectCollection()
    with open(os.path.join(dir_path, "build/uniform1600.txt")) as fp:
        lines = fp.readlines()
        for line in lines:
            line = line.split()
            collection.append(float(line[0]), float(line[1]))
    assert collection.maxDim() == pytest.approx(0.9999, 0.0001)
    assert abs(collection) == pytest.approx(410.455, 0.1)
    square = riss.RISSPacker(collection.cpp_collection) 
    assert abs(square) == pytest.approx(575.9, 0.1) 
