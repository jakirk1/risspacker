# RISSPacker (Rectangles Into a Single Square)

## Overview

This project provides an optimised implementation of the RISSPacker class from the [*FFD-Packer project*](https://gitlab.com/jakirk1/FFD-Packer) as a python module.

## Problem details

RISSPacker implements offline packing of rectangles into a single square.
For further discussion of this problem refer to [Martello et al. (2015)](https://www.sciencedirect.com/science/article/abs/pii/S0305054815001161).
The algorithm complexity has an upper bound of **O(nlogn)** where **n** is the number of rectangles to be packed.
Using an intel i5-3317U CPU and a few seconds of computation time, RISSPacker can pack more than a million rectangles with dimensions constructed from two independent uniform integer distributions into a perfect square with a packing fraction in excess of 0.99.

This inexact method can give reasonable performance for suitably large instance size (see the example jupyter notebook).
Note that as the instance size is reduced, more variability in the packing fraction performance can be expected.
The basic method is to solve the bin packing problem for the set of rectangles, and then arrange the set of resultant 'small' (in the current case square) bins inside a single 'large' square, aiming to minimise the 'large' square dimension.
The simple method that we employ to arrange a final set of **2**-dimensional bins into an upper bound smallest perfect square tends to perfect packing for a large number, **m -> \infty**, of bins.
The lost packing fraction due to this method is bounded by **(2m + 1)/(m^2 + 2m + 1)**.

## Usage

See the example jupyter notebook.

## Developers

Current developers:

* Jack Kirk - @jakirk1 (2020-)

## Getting Started with Development

Please read [CONTRIBUTING.md](/CONTRIBUTING.md) before you start working on this repository.

## Further reading

* D. S. Johnson, [*Near-optimal bin packing algorithms*](https://dspace.mit.edu/handle/1721.1/57819),  Ph.D. Thesis, Massachusetts Institute of Technology, Dept. of Mathematics, **1973**
* L. J. Guibas, R. Sedgewick, [*A dichromatic framework for balanced trees*](https://ieeexplore.ieee.org/document/4567957), 19th Annual Symposium on Foundations of Computer Science,**1978**
* S. Martello, and M. Monaci, [*Models and Algorithms for Packing Rectangles into the Smallest Square*](https://www.sciencedirect.com/science/article/abs/pii/S0305054815001161) Comp. Op. Res. Vol 63, **2015**, 161-171