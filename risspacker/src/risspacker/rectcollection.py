# The only purpose of the class in this file (RectCollection) is to allow
# iterations over rectangle items in a C++ RectCollection instance.

from risspacker import packer


class RectCollection():
    def __init__(self):
        self.cpp_collection = packer.RectCollection()

    def __len__(self):
        return len(self.cpp_collection)

    def __getitem__(self, position):
        return self.cpp_collection[position]

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n < len(self.cpp_collection):
            self.n += 1
            return self[self.n - 1]
        raise StopIteration

    def __abs__(self):
        return abs(self.cpp_collection)

    def maxDim(self):
        return self.cpp_collection.maxDim()

    def append(self, width, height):
        return self.cpp_collection.append(width, height)
