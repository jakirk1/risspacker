/*! \file BinItem.hpp
* File containing the BinItem and Rect structs.
 
* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/
#ifndef BINITEM_HPP
#define BINITEM_HPP

#ifdef PYTHON_BINDINGS
#include <pybind11/pybind11.h>
namespace py = pybind11;
#endif
struct BinItem {

    float width{ 0.0 };
    float pos{ 0.0 };
};

struct Rect : BinItem {

    float height;
    float posY { 0.0 };

    Rect(const float& _width, const float& _height)
        : height(_height)
    {
        width = _width;
    }
};

#ifdef PYTHON_BINDINGS
void export_Rect(py::module& m);
#endif
#endif