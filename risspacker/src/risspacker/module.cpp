#include <pybind11/pybind11.h>
#include "./RISSPacker.cpp"
#include "./BinItem.hpp"
#include "./RectCollection.hpp"

namespace py = pybind11;

PYBIND11_MODULE(packer, m)
{

    export_RISSPacker(m);
    export_Rect(m);
    export_RectCollection(m);
}
