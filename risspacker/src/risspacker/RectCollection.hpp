/*! \file RectCollection.hpp
* File containing the RectCollection class contains a vector of rectangles.

* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#ifndef RECTCOLLECTION_HPP
#define RECTCOLLECTION_HPP
#include "BinItem.hpp"
#include <vector>
#include <string>

class RectCollection {

public:
    RectCollection();
    float getMaxDim() const;
    float getTotalArea() const;
    void append(const float& width, const float& height);
    Rect get(const int& i) const;
    int numRects() const;

    std::vector<Rect> rects;

private:
    const std::string file;
    float totalArea;
    float maxDim;
};

#ifdef PYTHON_BINDINGS
void export_RectCollection(py::module& m);
#endif
#endif