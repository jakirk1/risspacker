/*! \file Bin.hpp
* File containing the Shelf and Shelves structs.

* Copyright (c) 2020-2021 FFD-Packer contributors
<https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause
License.
*/

#ifndef BIN_HPP
#define BIN_HPP
#include "BinItem.hpp"
#include <vector>

struct Shelf : BinItem {

    std::vector<Rect> items;
};

struct Shelves {

    std::vector<Shelf> items;
};
#endif