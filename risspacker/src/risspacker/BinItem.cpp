#include "BinItem.hpp"
#include <pybind11/pybind11.h>

namespace py = pybind11;

void export_Rect(py::module& m)
{
    py::class_<BinItem>(m, "BinItem")
        .def(py::init<>())
        .def_readwrite("width", &BinItem::width)
        .def_readwrite("pos", &BinItem::pos);

    py::class_<Rect, BinItem>(m, "Rect")
        .def(py::init<const float&, const float&>())
        .def_readwrite("height", &Rect::height)
        .def_readwrite("posY", &Rect::posY);
}
