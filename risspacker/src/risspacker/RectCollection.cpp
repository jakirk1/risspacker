// Copyright(c) 2020 - 2021 FFD-Packer contributors < https://gitlab.com/jakirk1/FFD-Packer>
// This file is part of the FFD-Packer project, released under the BSD 3 - Clause License.
#include "RectCollection.hpp"
#include <stdexcept>

#ifdef PYTHON_BINDINGS

#include <pybind11/pybind11.h>

namespace py = pybind11;
#endif

RectCollection::RectCollection()
    : totalArea(0.0)
    , maxDim(0.0)
{
}

void RectCollection::append(const float& width, const float& height)
{
    if ((width < 0) || (height < 0)) {
        throw std::runtime_error("Each rectangle dimension must be greater than zero.");
    }
    rects.push_back({ width, height });
    totalArea += height * width;
    if (height > maxDim) {
        maxDim = height;
    }
    if (width > maxDim) {
        maxDim = width;
    }
}

float RectCollection::getMaxDim() const
{
    return maxDim;
}

Rect RectCollection::get(const int& i) const
{
    return rects[i];
}

float RectCollection::getTotalArea() const
{
    return totalArea;
}

int RectCollection::numRects() const
{
    return rects.size();
}


#ifdef PYTHON_BINDINGS
void export_RectCollection(py::module& m)
{
    py::class_<RectCollection>(m, "RectCollection")
        .def(py::init<>())
        .def("__getitem__", &RectCollection::get)
        .def("append", &RectCollection::append)
        .def("__len__", &RectCollection::numRects)
        .def("__abs__", &RectCollection::getTotalArea)
        .def("maxDim", &RectCollection::getMaxDim);
}
#endif