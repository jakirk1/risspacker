/*! \file RISSPacker.hpp
* File containing the RISSPacker (Rectangles Into a Smallest Square Packer) class.

* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#ifndef RISSPACKER_HPP
#define RISSPACKER_HPP
#ifdef __linux__
#include <math.h>
#endif

#include "Bin.hpp"
#include "BinPacker.hpp"
#include "RectCollection.hpp"
#include <algorithm>
#include <memory>

class RISSPacker {
public:
    RISSPacker(RectCollection& _collection, const float& minShelvesPerBin);
    int getNumBins() const;
    float getSquareArea() const;
    float getBinDim() const;

private:
    int nBins;
    float squareArea;
    float binDim;

    RectCollection& collection;

    void run(const float& minShelvesPerBin);
    void saveSquare(const std::vector<Shelves>& sqBins, const float& binDim);
    void saveBin(const Shelves& bin, float&& posX, float&& posY);
};

#ifdef PYTHON_BINDINGS
void export_RISSPacker(py::module& m);
#endif
#endif
