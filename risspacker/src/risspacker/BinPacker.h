/*! \file BinPacker.h
* File declaring the BinPacker template that packs BinItems: Rect or Shelf (T1), into a vector of Bins: Shelf or Shelves (T2). 

  Pack() uses a Red-Black Johnson Tree to implement the packing in O(nlogn). 

* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#ifndef BINPACKER_H
#define BINPACKER_H
#include "Node.hpp"
#include "Bin.hpp"

template <class T1, class T2>
class BinPacker {
public:
    BinPacker(const float& _binDim);

    ~BinPacker();

    void pack(std::vector<T1>& items);
    std::vector<T2> bins;

private:
    Node* head;
    const float binDim;

    void findSpace(T1& binItem, Node* node);
    void add(T1& binItem, Node* node);
    void goRight(T1& binItem, Node* node);
    void split(T1& binItem, Node* node);

    void redBlkConstraint(Node* node);
    void swapColour(Node* node);
    void recolour(Node* node);
    void leftRotation(Node* node);

    void updateTree(Node* node);
};
#endif
