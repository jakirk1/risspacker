// Copyright(c) 2020 - 2021 FFD-Packer contributors < https://gitlab.com/jakirk1/FFD-Packer>
// This file is part of the FFD-Packer project, released under the BSD 3 - Clause License.

#ifndef BINPACKER_HPP
#define BINPACKER_HPP
#include "BinPacker.h"

template <class T1, class T2>
BinPacker<T1, T2>::BinPacker(const float& _binDim)
    : binDim(_binDim)
{
    head = new Node(binDim, -1, nullptr, true, false);

    bins.push_back(T2());
    bins.push_back(T2());
    head->left = new Node(binDim, 0, head, true, true);
    head->right = new Node(binDim, 1, head, false, true);
}

template <class T1, class T2>
BinPacker<T1, T2>::~BinPacker()
{
    delete head;
}

template <class T1, class T2>
void BinPacker<T1, T2>::pack(std::vector<T1>& items)
{
    for (auto& item : items) {
        if ((item.width < head->gap)) {
            findSpace(item, head);
        }
        else //need to insert a new "node".
        {
            goRight(item, head);
        }
    }
}

template <class T1, class T2>
void BinPacker<T1, T2>::findSpace(T1& item, Node* node)
{
    if (node->left != nullptr) {
        if (item.width < node->left->gap) {
            findSpace(item, node->left);
        }
        else {
            findSpace(item, node->right);
        }
    }

    else {
        add(item, node);
    }
}

template <class T1, class T2>
void BinPacker<T1, T2>::add(T1& item, Node* node)
{
    item.pos = binDim - node->gap;
    node->gap -= item.width;
    bins[node->id].items.push_back(item);

    if (!node->leftFlag)
        updateTree(node->up);
}

template <class T1, class T2>
void BinPacker<T1, T2>::updateTree(Node* node)
{
    if (node->right->gap < node->left->gap) {

        Node* oldLeft = node->left;

        node->left = node->right;
        node->right = oldLeft;

        node->left->leftFlag = true;
        node->right->leftFlag = false;
    }
    node->gap = node->right->gap;

    if (!node->leftFlag) {
        updateTree(node->up);
    }
}

template <class T1, class T2>
void BinPacker<T1, T2>::goRight(T1& item, Node* node)
{
    if (node->right != nullptr) {

        goRight(item, node->right);
    }
    else {
        split(item, node);
    }
}

template <class T1, class T2>
void BinPacker<T1, T2>::split(T1& item, Node* node)
{
    auto* stem = new Node(binDim, -1, node->up, false, node->red);

    node->up->right = stem;
    node->up = stem;

    stem->left = node;
    stem->left->leftFlag = true;
    stem->left->red = true;

    bins.push_back(T2());
    stem->right = new Node(binDim, static_cast<int>(bins.size() - 1), stem, false, true);
    add(item, stem->right);

    if (stem->red) {
        redBlkConstraint(stem);
    }
}

template <class T1, class T2>
void BinPacker<T1, T2>::redBlkConstraint(Node* node)
{

    if (node->up->left->red) {
        recolour(node);
    }
    else {
        leftRotation(node);
    }
}

template <class T1, class T2>
void BinPacker<T1, T2>::recolour(Node* node)
{
    node->red = false;
    node->up->left->red = false;

    if (node->up->up != nullptr) // not head node
    {
        swapColour(node->up);
        if (node->up->up->red) {
            redBlkConstraint(node->up->up);
        }
    }
}

template <class T1, class T2>
void BinPacker<T1, T2>::swapColour(Node* node)
{
    if (node->red) {
        node->red = false;
    }

    else {
        node->red = true;
    }
}

template <class T1, class T2>
void BinPacker<T1, T2>::leftRotation(Node* node)
{
    swapColour(node->up);
    swapColour(node);

    ///node->up->left/right can be immediately readdressed as appropriate.
    if (node->left->gap > node->up->left->gap) {
        node->up->right = node->left;
    }
    else {
        node->up->right = node->up->left;
        node->up->left = node->left;
    }

    node->up->right->leftFlag = false;
    node->up->left->up = node->up;
    node->up->right->up = node->up;
    node->up->gap = node->up->right->gap;

    // now node->left is free to be readdressed.
    // perform rotation:
    node->left = node->up;
    node->left->leftFlag = true;

    // now node->up is free to be readdressed.
    if (node->up->up != nullptr) {
        node->up = node->up->up;
        node->up->right = node;
    }
    else {
        node->up = nullptr;
        head = node;
        head->leftFlag = true;
    }
    // now finally node->left->up is free for readdressing.
    node->left->up = node;
}
#endif