// Copyright(c) 2020 - 2021 FFD-Packer contributors < https://gitlab.com/jakirk1/FFD-Packer>
// This file is part of the FFD-Packer project, released under the BSD 3 - Clause License.
#include "RISSPacker.hpp"

#ifdef PYTHON_BINDINGS

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;
#endif

RISSPacker::RISSPacker(RectCollection& _collection, const float& minShelvesPerBin)
    : nBins(0)
    , collection(_collection)

{
    if (collection.rects.empty()) {
        throw std::runtime_error("The rectangle collection is empty.  Add some rectangles before packing.");
    }
    run(minShelvesPerBin);
}

void RISSPacker::run(const float& minShelvesPerBin)
{
    std::sort(collection.rects.begin(), collection.rects.end(), [](const Rect& lhs, const Rect& rhs) { return lhs.height > rhs.height; });

    binDim = collection.getMaxDim() * minShelvesPerBin;

    ////////////////
    // Pack rects //
    ////////////////

    auto packer = std::make_unique<BinPacker<Rect, Shelf> >(binDim);
    packer->pack(collection.rects);
    collection.rects.clear();
    float totalWidth = 0.0;
    for (auto& shelf : packer->bins) {
        if (!shelf.items.empty()) {
            shelf.width = shelf.items[0].height;
        }
        else {
            shelf.width = 0;
        }
        totalWidth += shelf.width;
    }
    ///

    ///////////////////////////
    // Pack shelves of rects //
    ///////////////////////////
    if (totalWidth > binDim) {
        auto shelfPacker = std::make_unique<BinPacker<Shelf, Shelves> >(binDim);
        shelfPacker->pack(packer->bins);
        packer.reset();

        try {
            saveSquare(shelfPacker->bins, binDim);
        }
        catch (std::runtime_error const& e) {
            throw;
        }
        nBins = shelfPacker->bins.size();
        shelfPacker.reset();
    }
    else {

        float gap = binDim;
        for (auto& shelf : packer->bins) {
            shelf.pos = binDim - gap;
            gap -= shelf.width;
        }
        std::vector<Shelves> bin;
        bin.push_back(Shelves());
        bin[0].items = packer->bins;

        try {
            saveSquare(bin, binDim);
        }
        catch (std::runtime_error const& e) {
            throw;
        }
        nBins = 1;
        packer.reset();
    }
}

void RISSPacker::saveSquare(const std::vector<Shelves>& sqBins, const float& binDim)
{
    auto n = static_cast<int>(floor(sqrt(sqBins.size())));
    auto nSq = n * n;
    auto extra = static_cast<int>(sqBins.size()) - nSq;
    auto sideLength = binDim * static_cast<float>(n);

    if (extra > 0) {
        sideLength += binDim;
    }

    squareArea = pow(sideLength, 2);

    for (auto i = 0; i < n; i++) {
        for (auto j = 0; j < n; j++) {
            auto ID = n * i + j;
            saveBin(sqBins[ID], static_cast<float>(i) * binDim, static_cast<float>(j) * binDim);
        }
    }

    auto l = 0;

    if (extra % 2) {

        l = static_cast<int>(floor((extra / 2)));
    }
    else {
        l = extra / 2;
    }

    //cover top of square
    for (auto i = nSq; i < nSq + l; i++) {
        saveBin(sqBins[i], static_cast<float>(i - nSq) * binDim, static_cast<float>(n) * binDim);
    }

    //cover side of square
    for (auto i = nSq + l; i < nSq + extra; i++) {
        saveBin(sqBins[i], static_cast<float>(n) * binDim, static_cast<float>(i - (nSq + l)) * binDim);
    }
}

void RISSPacker::saveBin(const Shelves& bin, float&& posX, float&& posY)
{
    for (const auto& shelf : bin.items) {
        for (const auto& rect : shelf.items) {
            collection.rects.push_back(Rect(rect.width, rect.height));
            collection.rects[collection.rects.size() - 1].pos = std::move(posX);
            collection.rects[collection.rects.size() - 1].posY = std::move(posY);
            collection.rects[collection.rects.size() - 1].pos += rect.pos;
            collection.rects[collection.rects.size() - 1].posY += shelf.pos;
        }
    }
}

int RISSPacker::getNumBins() const
{
    return nBins;
}

float RISSPacker::getSquareArea() const
{
    return squareArea;
}

float RISSPacker::getBinDim() const
{
    return binDim;
}

#ifdef PYTHON_BINDINGS
void export_RISSPacker(py::module& m)
{
    py::class_<RISSPacker>(m, "RISSPacker")
        .def(py::init<RectCollection&, const float&>())
        .def("__len__", &RISSPacker::getNumBins)
        .def("__abs__", &RISSPacker::getSquareArea)
        .def("binDim", &RISSPacker::getBinDim);
}
#endif