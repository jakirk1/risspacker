# Contributing to the hyper-cube-fastpack repository

## Getting started

hyper-cube-fastpack can be immediately built in both windows and linux using the CMakeLists.txt in the hyper-cube-fastpack/src/ folder.
However, currently only a linux build and unit testing is performed in the CI pipelines.
It is recommended to use the (Windows Subsystem for Linux) to carry out development (tested with ubuntu 20.04).
This allows easy continuous development in both windows and linux.
As a general guide developers should aim to ensure that the codebase remains cross-platform in the simplest possible manner by using cross-platform standard libraries where possible.

### Clone this repository

From the command line, run:

```sh
git clone git@gitlab.com:jakirk1/hyper-cube-fastpack.git
cd hyper-cube-fastpack
```

### Installing required packages

hyper-cube-fastpack requires:

* [CMake](https://cmake.org/), [clang-format](https://clang.llvm.org/docs/ClangFormat.html), and [Catch 2](https://github.com/catchorg/Catch2)

You will need to install all of these in your development machine.
If you are running Ubuntu 20.04, this should work for you:

```sh
sudo apt-get install build-essential catch cmake clang-format
```

### Formatting your code

We use [clang-format](https://clang.llvm.org/docs/ClangFormat.html) to check that all C++ code is formatted using the **WebKit** style.
It is a good idea to set your editor or IDE up to automatically format your C++ code whenever you save a file.

The steps to do this will vary between editors, but in SublimeText3, install the [Clang Format](https://packagecontrol.io/packages/Clang%20Format) plugin.
Next go to **Preferences -> Package Settings -> Clang Format -> Settings - User** and add this to the resulting file:

```json
{
    "format_on_save": true,
    "style": "WebKit"
}
```

### Ensuring that your Markdown syntax is valid

To make sure that the Markdown documentation in this repository is valid, please use [the mdl Markdown lint](https://github.com/markdownlint/markdownlint).

To install the lint on Debian-like machines, use the [Rubygems](https://rubygems.org/) package manager:

```sh
sudo apt-get install ruby
sudo apt-get install gem
sudo gem install mdl
```

Because we keep each sentence on a separate line, you will want to suppress spurious `MD013 Line length` reports by configuring `mdl`.
The file [.mdl.rb](/.mdl.rb) contains styles that deal with `MD013` and other tweaks we want to make to the lint.
To use the style configuration, pass it as a parameter to `mdl` on the command line:

```sh
mdl -s .mdl.rb DOCUMENT.md
```

If you want to run `mdl` from your IDE or editor, you will either need to configure it, or find a plugin, such as [this one for Sublime Text](https://github.com/SublimeLinter/SublimeLinter-mdl).

### Setting up a git hook

This repository provides [git hooks](https://githooks.com/) that will run Markdown and C++ lints and unit tests.

To install the hooks, run the hook install script:

```sh
./bin/create-hook-symlinks
```

## Further reading

* [CMake](https://cmake.org/)
* [Catch 2](https://github.com/catchorg/Catch2)
* [clang-format](https://clang.llvm.org/docs/ClangFormat.html)
* [git hooks](https://githooks.com/)
* [mdl](https://github.com/markdownlint/markdownlint)
